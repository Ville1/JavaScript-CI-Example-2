var rng = require("../src/RNG.js");

describe("RNG tests", function() {
	describe("Initialization", function() {
		it("Valid parameters", function() {
			expect(rng.init(1, 4, "int")).toBe(true);
			expect(rng.init(10.5, 50, "float")).toBe(true);
		});
		
		it("Invalid parameters", function() {
			expect(rng.init(10, undefined, "float")).toBe(false);
			expect(rng.init(null, 5.75, "int")).toBe(false);
			expect(rng.init("abc", 1, "int")).toBe(false);
			expect(rng.init(false, 1, "int")).toBe(false);
			expect(rng.init(1, 10, null)).toBe(false);
		});
	});
	
	describe("Generation", function() {
		it("Int 1 - 10", function() {
			rng.init(1, 10, "int");
			var loops = 10;
			for(var i = 0; i < loops; i++) {
				var number = rng.random();
				expect(number >= 1 && number <= 10 && number % 1 === 0).toBeTruthy();
			}
		});
		
		it("Float 10 - 100", function() {
			rng.init(10, 100, "float");
			var loops = 10;
			for(var i = 0; i < loops; i++) {
				var number = rng.random();
				expect(number >= 10 && number <= 100).toBeTruthy();
			}
		});
		
		it("Float -7.5 - 2.5", function() {
			rng.init(-7.5, 2.5, "float");
			var loops = 10;
			for(var i = 0; i < loops; i++) {
				var number = rng.random();
				expect(number >= -7.5 && number <= 2.5).toBeTruthy();
			}
		});
		
		it("NaN", function() {
			rng.init(NaN, 1, "float");
			var loops = 10;
			for(var i = 0; i < loops; i++) {
				var number = rng.random();
				expect(isNaN(number)).toBeTruthy();
			}
		});
	});
});