var min;
var max;
var type;

exports.init = function(min_p, max_p, type_p) {
	if(typeof min_p !== "number" || typeof max_p !== "number" || min_p > max_p || !type_p)
		return false;
	min = min_p;
	max = max_p;
	if(type_p === "int" || type_p === "float")
		type = type_p;
	else
		type = "int";
	return true;
}

exports.random = function() {
	var result = (Math.random() * (max - min)) + min;
	if(type == "int")
		result = Math.round(result);
	return result;
}
